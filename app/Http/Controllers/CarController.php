<?php

namespace App\Http\Controllers;

use App\car;
use Illuminate\Http\Request;
use App\Helpers\JwtAuth;
use Illuminate\Support\Facades\Validator;

class CarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cars = Car::all();
        return response()->json(array(
            'cars' => $cars,
            'status' => 'success'
        ), 200);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);die;
        $hash = $request->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if ($checkToken) {
            //Recoger los datos por POST

            $json = $request->input('json', null);
            //dd($json);die();
            //$params_array = json_decode($json, true);

            // Conseguir el usuario identificado
            $user = $jwtAuth->checkToken($hash, true);

            //Validación

            $validate = Validator::make($json, [
                'title' => 'required|min:5',
                'description' => 'required',
                'price' => 'required',
                'status' => 'required'
            ]);

            if ($validate->fails()) {
                return response()->json($validate->errors(), 400);
            }

            //Guardar el Carro
            $car = new Car();
            //dd($json[('title')]);die();
            $car->user_id = $user->id;
            $car->title = $json[('title')];
            $car->description = $json[('description')];
            $car->price = $json[('price')];
            $car->status = $json[('status')];

            $car->save();

            $data = array(
                'status' => 'success',
                'code' => '200',
                'car' => $car
            );
        } else {
            //Devolver error
            $data = array(
                'status' => 'error',
                'code' => '400',
                'message' => 'Login incorrecto'
            );
            return response()->json($data, 400);
        }
        return response()->json($data, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\car  $car
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $car = Car::find($id)->load('user');

        return response()->json(array(
            'car' => $car,
            'status' => 'success'
        ), 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\car  $car
     * @return \Illuminate\Http\Response
     */
    public function edit(car $car)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\car  $car
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $hash = $request->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if ($checkToken) {
            //recoger los parametros por POST
            $json = $request->input('json', null);

            //Validar los datos
            $validate = Validator::make($json, [
                'title' => 'required|min:5',
                'description' => 'required',
                'price' => 'required',
                'status' => 'required'
            ]);

            if ($validate->fails()) {
                return response()->json($validate->errors(), 400);
            }

            //Actualizar el registro
            $car = Car::where('id', $id)->update($json);

            $data = array(
                'car' => $car,
                'status' => 'success',
                'code' => '200',
                'message' => 'Auto Actualizado!!'
            );
        } else {
            //Devolver error
            $data = array(
                'status' => 'error',
                'code' => '400',
                'message' => 'Login incorrecto'
            );
            return response()->json($data, 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\car  $car
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $hash = $request->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if ($checkToken) {
            //comprobar que el usuario exista
            $car = Car::find($id);

            //borrarlo
            $car->delete();
        } else {
            //Devolver error
            $data = array(
                'status' => 'error',
                'code' => '400',
                'message' => 'Login incorrecto'
            );
            return response()->json($data, 400);
        }
    }
}

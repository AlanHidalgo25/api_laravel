<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Helpers\JwtAuth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //Recoger POST - datos del usuario
        $json = $request->input('json');
        // $params = $request->input('json');
        // $params = json_encode($json);
        // // dd($params);
        // $params = json_decode($json);
        //dd($json);die();

        //convertir el Json traido del input en un arreglo
        $email = (!is_null($json) && isset($json['email'])) ? $json['email'] : null;
        $name = (!is_null($json) && isset($json['name'])) ? $json['name'] : null;
        $surname = (!is_null($json) && isset($json['surname'])) ? $json['surname'] : null;
        $role = 'ROLE_USER';
        $password = (!is_null($json) && isset($json['password'])) ? $json['password'] : null;

        if (!is_null($email) && !is_null($password) && !is_null($name)) {
            //creación de usuario
            $user = new User();
            $user->email = $email;
            $user->password = $password;
            $user->name = $name;
            $user->surname = $surname;
            $user->role = $role;
            $pwd = hash('sha256', $password);
            $user->password = $pwd;

            //comprobar usuario duplicado
            $isset_user = User::where('email', '=', $email)->first();

            if (count((array) $isset_user) == 0) {
                //guarda el usuario
                $user->save();
                $data = array(
                    'status' => 'success',
                    'code' => '200',
                    'message' => 'Usuario Registrado'
                );
            } else {
                //no guarda el usuario
                $data = array(
                    'status' => 'error',
                    'code' => '300',
                    'message' => 'Usuario duplicado'
                );
            }
        } else {
            $data = array(
                'status' => 'error',
                'code' => '400',
                'message' => 'Usuario no creado'
            );
        }
        return response()->json($data, 200);
    }

    public function login(Request $request)
    {
        $jwtAuth = new JwtAuth();

        //Recibir POST
        $json = $request->input('json', null);
        $email = (!is_null($json) && isset($json['email'])) ? $json[('email')] : null;
        $password = (!is_null($json) && isset($json['password'])) ? $json[('password')] : null;
        $getToken = (!is_null($json) && isset($json['gettoken'])) ? $json[('gettoken')] : null;

        //cifrar la password

        $pwd = hash('sha256', $password);

        if (!is_null($email) && !is_null($password) && ($getToken == null || $getToken == 'false')) {

            $signup = $jwtAuth->signup($email, $pwd);
        } elseif ($getToken != null) {
            //dd($getToken);die();

            $signup = $jwtAuth->signup($email, $pwd, $getToken);
        } else {

            $signup = array(
                'status' => 'error',
                'message' => 'Envia tus datos por POST'
            );
        }
        return response()->json($signup, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
